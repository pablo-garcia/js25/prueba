//importamos los metodos de tasks
const { taskOne, taskTwo } = require('./tasks');

async function main(){
  //medimos el tiempo que tarda
  console.time('midiendo el tiempo')

  const val1 = await taskOne();
  const val2 = await taskTwo();

  console.timeEnd("midiendo el tiempo");

  console.log('task1 return',val1);
  console.log('task2 return',val2);


}

main();
