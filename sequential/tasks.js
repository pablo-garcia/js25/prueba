const util = require('util');
const sleep = util.promisify(setTimeout);

 module.exports = {
  async taskOne(){
    //crea un error u lanzalo y para
    try {
      throw new Error('Some Problemo');

      await sleep(4000);
      return 'ONE VALUÊ';

    } catch (e) {
      console.log(e)
    }


  },
  async taskTwo(){
    try {

      await sleep(2000);
      return 'TWO VALUE';

    } catch (e) {
        console.log(e);
    }


  }
};
