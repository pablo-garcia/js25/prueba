//importamos los metodos de tasks
const { taskOne, taskTwo } = require('./tasks');

async function main(){
  //medimos el tiempo que tarda
  console.time('midiendo el tiempo')
  //retorna arreglo y ejecuta de manera paralela
  const result = await Promise.all([taskOne(),taskTwo()]);
  console.timeEnd("midiendo el tiempo");

  console.log('task1 return',result[0]);
  console.log('task2 return',result[1]);


}

main();
